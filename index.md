---
title: Home
layout: home
nav_order: 1
---
<img width="150px" src="images/k8s.svg">

# Debian Kubernetes Team

---

## About

The **Debian Kubernetes Team** aims to empower users with the knowledge and resources needed to harness the full potential of Kubernetes in Debian environments, promoting efficiency, reliability, and innovation in containerized application deployment and orchestration.

---


[QA](https://qa.debian.org/developer.php?email=team%2Bkubernetes%40tracker.debian.org) | [Tracker](https://tracker.debian.org/teams/kubernetes) | [Wiki](https://wiki.debian.org/Teams/pkg-kubernetes) | [Backlog](https://salsa.debian.org/kubernetes-team/docs/-/issues/1)
- Team Email: **team+kubernetes@tracker.debian.org**

---

## Ultimate Debian Database (UDD)
- [HTML](https://udd.debian.org/lintian/?email1=&email2=&email3=team%2Bkubernetes%40tracker.debian.org&packages=&ignpackages=&format=html&lt_error=on&lt_warning=on&lintian_tag=#all)
- [JSON](https://udd.debian.org/lintian/?email1=&email2=&email3=team%2Bkubernetes%40tracker.debian.org&packages=&ignpackages=&format=json&lt_error=on&lt_warning=on&lintian_tag=#all)
