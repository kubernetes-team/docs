import psycopg2
import yaml
import json
from psycopg2 import sql

def query_udd(query: str):

    db_config = {
        'dbname': 'udd',
        'user': 'udd-mirror',
        'password': 'udd-mirror',
        'host': 'udd-mirror.debian.net',
        'port': '5432'
    }

    try:
        conn = psycopg2.connect(**db_config)
        conn.set_client_encoding('UTF8')

        cursor = conn.cursor()
        cursor.execute(query)

        results = cursor.fetchall()
        colnames = [desc[0] for desc in cursor.description]

        cursor.close()
        conn.close()

        return [dict(zip(colnames, row)) for row in results]

    except psycopg2.Error as e:
        print(f"Error: {e}")
        return None


if __name__ == "__main__":
    with open("_data/packages.yaml") as stream:
        try:
            packages_data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    for source in packages_data.keys():
        sources_query = f"""
        SELECT DISTINCT ON (version) *
        FROM sources
        WHERE source='{source}'
        """
        source_results = query_udd(sources_query)
        releases = []
        try:
            latest = source_results[-1]
        except IndexError as e: # Package not available yet skipping
            continue

        for result in source_results:
            releases.append({
                'version': result['version'],
                'release': result['release']
            })

        packages_data[source]['version'] = latest['version']
        packages_data[source]['arch'] = latest['architecture']
        packages_data[source]['standards_version'] = latest['standards_version']
        packages_data[source]['releases'] = releases

        upstream_query = f"""
        SELECT DISTINCT ON (version) *
        FROM upstream
        WHERE source='{source}'
        """
        upstream_results = query_udd(upstream_query)
        try:
            upstream = upstream_results[-1]

            packages_data[source]['upstream'] = {}
            packages_data[source]['upstream']['version'] = upstream['version']
            packages_data[source]['upstream']['status'] = upstream['status']
            packages_data[source]['upstream']['last_check'] = upstream['last_check'].strftime("%Y-%m-%d %H:%M:%S")
        except IndexError as e: # Package not available yet skipping
            continue


        bugs_query = f"""
        SELECT *
        FROM bugs
        WHERE source='{source}'
        """
        bugs_results = query_udd(bugs_query)
        packages_data[source]['bugs'] = []
        for bug in bugs_results:
            bug_obj = {
                'id': bug['id'],
                'arrival': bug['arrival'],
                'status': bug['status'],
                'severity': bug['severity'],
                'submitter': {
                    'name': bug['submitter_name'],
                    'email': bug['submitter_email']
                },
                'title': bug['title'],
                'last_modified': bug['last_modified']
            }
            packages_data[source]['bugs'].append(bug_obj)


    with open("_data/packages.yaml", 'w', encoding='utf8') as outfile:
        yaml.dump(packages_data, outfile, default_flow_style=False, allow_unicode=True)

