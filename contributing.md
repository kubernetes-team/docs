---
title: Contributing
layout: home
nav_order: 3
---

# Contributing

{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}


Kubernetes major programming language is Golang and because of that most of tools to interact with cluster were also created using Golang.

Debian already has a Golang team but it order to find a place just for Kubernetes things the Debian Kubernetes team was created.

But keep in mind that we still very connected to debian go team using their packaging tools, as still packagaging and keeping libraries in there.

---

## Join team

If you want to join the the Debian Go Packaging Team, please head over to our Salsa group page at [https://salsa.debian.org/kubernetes-team](https://salsa.debian.org/kubernetes-team) and click the `Request Access` link. You will need a Salsa account for this. You may also send a mail to the team’s mailing list to ask one of the owners to accept your request.

Feel free to contact us on `#debian-kubernetes` on [irc.debian.org](irc.debian.org) (webchat). You can also subscribe to our mailing list debian-go and write to us there.

---

## Tools

- [Introduction to dh-make-golang](https://people.debian.org/~stapelberg/2015/07/27/dh-make-golang.html) — use dh-make-golang to create initial packaging for Go libraries/programs
- See also the [dh-make-golang GitHub page](https://github.com/Debian/dh-make-golang)
- [Debian Go Packaging](https://go-team.pages.debian.net/packaging.html)

---

## Team Maintenance

All Kubenetes related packages excluding libraries are `team-maintained` in the Kubenetes team. This has multiple advantages:
  - There are no single points of failure, if one maintainer cannot continue maintaining a package, another maintainer can easily help out or take over.
  - All packages can share the same technical standards and workflows.
  - When working within a team anyway, it is much easier to get help on any technical issue.

A package maintained within the team should have the name of the team either in the Maintainer field or in the Uploaders field. We use tracker.debian.org’s team functionality and spell the team like this:

```
Debian Kubernetes Packaging Team <team+kubernetes@tracker.debian.org>
```

This enables the team to have an overview of its packages on the DDPO website and ensures that all packages maintained by the team are automatically added to the Debian Package Tracker team.

Putting the team in Maintainers is a strong statement that fully collaborative maintenance is preferred. Anyone can commit to the git repository and upload as needed. A courtesy email to Uploaders can be nice but not required.

If for some reason, a package needs tighter control by its original maintainers, putting the team in Uploaders is a weak statement of collaboration. Help in maintaining the package is appreciated, commits to the git repository are freely welcomed, but before uploading, please contact the Maintainer for the green light.

---

## Packaging in git

All Go packages are maintained in git and must be buildable `gbp build-package` using [git-buildpackage](https://honk.sigxcpu.org/projects/git-buildpackage/manual-html/).

We use [Salsa](https://salsa.debian.org/kubernetes-team/packages/) to store the git repositories.


---

## Using dh-make-golang

Instead of copying the debian/ directory from some random other package, please use [dh-make-golang](https://people.debian.org/~stapelberg/2015/07/27/dh-make-golang.html) when you start packaging a new Go library/program.

Please do NOT use `dh-make-golang create-salsa-project` to create Salsa projects, it will create the repo in the go team Group instead of Kubernetes. 

---

## Version numbers

Many Go libraries (and also some actual Go programs) don’t have version numbers in the traditional sense, but live in a version control repository of some kind.

In case your upstream does not use version numbers, the Debian package version will look like this:

`0.0~git20130606.b00ec39-1`

- The 0.0 in the beginning is used to allow upstream to adopt version numbers at any point in time and also to make it clear that no traditional version number is used for this package.
- The second part is the version control system, e.g. `git`, `hg`, `svn`.
- Afterwards, a date follows in the format `YYYYMMDD`.
- After the dot, the version control system revision follows, to make it clear which commit was packaged, as many repositories have multiple commits on a given day.
- The last part after the dash is the Debian version number.

In case you make more than one snapshot per day, you can append a snapshot number after the date, e.g. `0.0~git20130606.2.b00ec39-1`. This should rarely be necessary.

---

## changelog: UNRELEASED

During the time when you still work on a package, i.e. before it is ready to upload, please put `UNRELEASED` into the distribution field in debian/changelog (dch -v <debian_version> will do it automatically). When the package is ready for uploading, change it to unstable (dch -r).

If you change something that has to be noted in debian/changelog, just add a line to the current entry (dch -a). The [firstname lastname] markers added by dch are okay to give credit to non-upload-permitted contributors (also for the initial changelog entry).

Important NOTES to other group members may be placed at the top of the current changelog entry of packages that are not yet ready for upload (e.g. why a package is still UNRELEASED, etc.).

---

## Binary-only packages

A binary-only package is a package that contains a program written in Go, but no source code. An example is `kind`, which is written in Go, but does not offer an API (thus no source code).

---

## Naming Conventions

The source package should be named like the upstream project, i.e. kind, you do NOT need to call it `golang-kind`.

Similarly, the resulting binary package(s) should `NOT` contain the `golang-` prefix.

---

## Use dh-golang

Install `dh-golang` from Debian unstable so that you are using the newest version. The buildds are using the unstable version, too, so this is important.

`dh-golang` comes with an example `debian/rules` file: https://salsa.debian.org/go-team/packages/dh-golang/-/blob/debian/sid/example/rules

You will need to change the value of `XS-Go-Import-Path` in `debian/control` to correspond to your program’s upstream package name. This is usually what you would go get when installing it manually. `dh-golang` needs that information so that it can run go install.

`dh-golang` sets up a build environment that contains all the libraries that are available in /usr/share/gocode/src, so you need to add Build-Depends to your package. As an example, Debian Code Search depends on golang-github-lib-pq-dev and others: https://github.com/Debian/dcs/blob/master/debian/control#L5

---

## Library only packages

Libraries written in Go are packaged for Debian with the only purpose of building other Go programs for Debian, and because of that we decided to keep those packages inside golang team. For more info about library packaging and naming convension check:

https://go-team.pages.debian.net/packaging.html

---

## Dependencies

Your library package, e.g. `golang-github-lib-pq-dev`, needs to have all the other Go libraries it depends on in its Depends line. The dependencies need to be available at build time to run the tests (if any) and at installation time so that other packages can be built.
