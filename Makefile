build:
	docker build -t debian-kubernetes-site --network host .

run:
	docker run -it -v $(PWD):/site --network host debian-kubernetes-site

clean:
	docker run -it -v $(PWD):/site --network host --workdir=/site --entrypoint bash ruby:3.3.0 bin/cleanup.sh

shell:
	docker run -it -v $(PWD):/site --network host --workdir=/site --entrypoint bash ruby:3.3.0
