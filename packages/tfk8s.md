---
layout: default
title: tfk8s
parent: Packages
nav_order: 5
---

# tfk8s

{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

`tfk8s` is a tool that makes it easier to work with the [Terraform Kubernetes Provider](https://github.com/hashicorp/terraform-provider-kubernetes).

---

## Information

{% include package_info.html source=page.title%}


---

## Releases

{% include package_releases.html source=page.title%}

---

## Installation

```bash
apt install tfk8s
```

---

## Completion

Completions will be installed for `bash`, `zsh` and `fish` shells.

---

## Usage

```
Usage of tfk8s:
  -f, --file string         Input file containing Kubernetes YAML manifests (default "-")
  -M, --map-only            Output only an HCL map structure
  -o, --output string       Output file to write Terraform config (default "-")
  -p, --provider provider   Provider alias to populate the provider attribute
  -s, --strip               Strip out server side fields - use if you are piping from kubectl get
  -Q, --strip-key-quotes    Strip out quotes from HCL map keys unless they are required.
  -V, --version             Show tool version
```

[![asciicast](https://asciinema.org/a/jSmyAg4Ar6EcwKCTCXN8iAJM2.svg)](https://asciinema.org/a/jSmyAg4Ar6EcwKCTCXN8iAJM2)


---

## Examples

Create Terraform configuration from YAML files

```bash
tfk8s -f input.yaml -o output.tf
```

or, using pipes:

```bash
cat input.yaml | tfk8s > output.tf
```

**input.yaml**

```yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: test
data:
  TEST: test
```

**output.tf**

```tf
resource "kubernetes_manifest" "configmap_test" {
  manifest = {
    "apiVersion" = "v1"
    "data" = {
      "TEST" = "test"
    }
    "kind" = "ConfigMap"
    "metadata" = {
      "name" = "test"
    }
  }
}
```

[See more examples](https://github.com/jrhouston/tfk8s?tab=readme-ov-file#examples)

{% assign bugs = site.data.packages[page.title].bugs %}
{% if bugs.size > 0 %}
---

## Bugs
{% endif %}

{% include package_bugs.html source=page.title%}

---

## Links

- [homepage](https://github.com/johanhaleby/tfk8s)
- [lintian](https://udd.debian.org/lintian/?packages=tfk8s)
- buildd: [logs](https://buildd.debian.org/status/package.php?p=tfk8s), [reproducibility](https://tests.reproducible-builds.org/debian/rb-pkg/tfk8s.html)
- [popcon](https://qa.debian.org/popcon.php?package=tfk8s)
- [browse source code](https://sources.debian.org/src/tfk8s/unstable/)
- [other distros](https://repology.org/tools/project-by?name_type=srcname&noautoresolve=on&repo=debian_unstable&target_page=project_packages&name=tfk8s)
