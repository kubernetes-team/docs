---
layout: default
title: kubernetes-split-yaml
parent: Packages
nav_order: 3
---

# kubernetes-split-yaml

{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

Split a giant yaml file into one file per Kubernetes resource

This program can be useful in case it's necessary to split a big Kubernetes yaml manifest files into small ones.

It supports filters using namespaced hierarchy, Kubernetes objects starting with a word or Deployments and StatefulSets types.


---

## Information

{% include package_info.html source=page.title%}


---

## Releases

{% include package_releases.html source=page.title%}

---

## Installation

```bash
apt install kubernetes-split-yaml
```

---

## Completion

Completions will be installed for `bash`, `zsh` and `fish` shells.

---

## Usage


Simple invocation

```bash
kubernetes-split-yaml giant-k8s-file.yaml
```

Modify / filter output filenames

```bash
# Note by default it'll output 0.2.0 non-hierical files
$ kubernetes-split-yaml --help

# Get namespaced hierarchy for output files
$ kubernetes-split-yaml --template_sel tpl_ns --outdir my-clustername/namespaces giant-k8s-file.yaml

# Ditto above, but only for Kubernetes objects starting with "myapp"
$ kubernetes-split-yaml --name_re ^myapp --template_sel tpl_ns --outdir my-clustername/namespaces giant-k8s-file.yaml

# Ditto above, but only for Deployments and StatefulSets
$ kubernetes-split-yaml --kind_re '^(StatefulSet|Deployment)' --name_re ^myapp --template_sel tpl_ns --outdir my-clustername/namespaces giant-k8s-file.yaml
```

{% assign bugs = site.data.packages[page.title].bugs %}
{% if bugs.size > 0 %}
---

## Bugs
{% endif %}

{% include package_bugs.html source=page.title%}

---

## Links

- [homepage](https://github.com/johanhaleby/kubernetes-split-yaml)
- [lintian](https://udd.debian.org/lintian/?packages=kubernetes-split-yaml)
- buildd: [logs](https://buildd.debian.org/status/package.php?p=kubernetes-split-yaml), [reproducibility](https://tests.reproducible-builds.org/debian/rb-pkg/kubernetes-split-yaml.html)
- [popcon](https://qa.debian.org/popcon.php?package=kubernetes-split-yaml)
- [browse source code](https://sources.debian.org/src/kubernetes-split-yaml/unstable/)
- [other distros](https://repology.org/tools/project-by?name_type=srcname&noautoresolve=on&repo=debian_unstable&target_page=project_packages&name=kubernetes-split-yaml)
