---
layout: default
title: kubetail
parent: Packages
nav_order: 4
---

# kubetail

{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

Bash script that enables you to aggregate **(tail/follow)** logs from multiple pods into one stream.
This is the same as running `kubectl logs -f` but for multiple pods.

---

## Information

{% include package_info.html source=page.title%}


---

## Releases

{% include package_releases.html source=page.title%}

---

## Installation

```bash
apt install kubetail
```

---

## Completion

Completions will be installed for `bash`, `zsh` and `fish` shells.

---

## Usage

First find the names of all your pods:

```bash
kubectl get pods
```

This will return a list looking something like this:

```
NAME                   READY     STATUS    RESTARTS   AGE
app1-v1-aba8y          1/1       Running   0          1d
app1-v1-gc4st          1/1       Running   0          1d
app1-v1-m8acl  	       1/1       Running   0          6d
app1-v1-s20d0  	       1/1       Running   0          1d
app2-v31-9pbpn         1/1       Running   0          1d
app2-v31-q74wg         1/1       Running   0          1d
my-demo-v5-0fa8o       1/1       Running   0          3h
my-demo-v5-yhren       1/1       Running   0          2h
```

{% assign bugs = site.data.packages[page.title].bugs %}
{% if bugs.size > 0 %}
---

## Bugs
{% endif %}

{% include package_bugs.html source=page.title%}

---

## Links

- [homepage](https://github.com/johanhaleby/kubetail)
- [lintian](https://udd.debian.org/lintian/?packages=kubetail)
- buildd: [logs](https://buildd.debian.org/status/package.php?p=kubetail), [reproducibility](https://tests.reproducible-builds.org/debian/rb-pkg/kubetail.html)
- [popcon](https://qa.debian.org/popcon.php?package=kubetail)
- [browse source code](https://sources.debian.org/src/kubetail/unstable/)
- [other distros](https://repology.org/tools/project-by?name_type=srcname&noautoresolve=on&repo=debian_unstable&target_page=project_packages&name=kubetail)
