---
layout: default
title: kubectx
parent: Packages
nav_order: 2
---

# kubectx

{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

This package provides both `kubectx` and `kubens` binaries.

- `kubectx` is a tool to switch between contexts (clusters) on kubectl faster.
- `kubens` is a tool to switch between Kubernetes namespaces (and configure them for kubectl) easily.

---

## Information

{% include package_info.html source=page.title %}


---

## Releases

{% include package_releases.html source=page.title %}

---

## Installation

```bash
apt install kubectx
```

---

## Completion

Both `kubectx` and `kubens` support Tab completion on **bash/zsh/fish** shells to help with long context names. You don't have to remember full context names anymore.

Completions will be installed for `bash`, `zsh` and `fish` shells.

---

## Usage

Here's a `kubectx` demo:

![](/docs/images/kubectx-demo.gif)

And here's a `kubens` demo:

![](/docs/images/kubens-demo.gif)

---

## Examples

```bash
# switch to another cluster that's in kubeconfig
$ kubectx minikube
Switched to context "minikube".

# switch back to previous cluster
$ kubectx -
Switched to context "oregon".

# rename context
$ kubectx dublin=gke_ahmetb_europe-west1-b_dublin
Context "gke_ahmetb_europe-west1-b_dublin" renamed to "dublin".

# change the active namespace on kubectl
$ kubens kube-system
Context "test" set.
Active namespace is "kube-system".

# go back to the previous namespace
$ kubens -
Context "test" set.
Active namespace is "default".
```

If you have [fzf](https://github.com/junegunn/fzf) installed, you can also **interactively** select a context or cluster, or fuzzy-search by typing a few characters. To learn more, read [interactive mode →](https://github.com/ahmetb/kubectx?tab=readme-ov-file#interactive-mode)


{% assign bugs = site.data.packages[page.title].bugs %}
{% if bugs.size > 0 %}
---

## Bugs
{% endif %}

{% include package_bugs.html source=page.title%}

---

## Links

- [homepage](https://github.com/johanhaleby/kubectx)
- [lintian](https://udd.debian.org/lintian/?packages=kubectx)
- buildd: [logs](https://buildd.debian.org/status/package.php?p=kubectx), [reproducibility](https://tests.reproducible-builds.org/debian/rb-pkg/kubectx.html)
- [popcon](https://qa.debian.org/popcon.php?package=kubectx)
- [browse source code](https://sources.debian.org/src/kubectx/unstable/)
- [other distros](https://repology.org/tools/project-by?name_type=srcname&noautoresolve=on&repo=debian_unstable&target_page=project_packages&name=kubetail)
