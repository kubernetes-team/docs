with open('pkgs') as f:
    pkgs = f.readlines()


for pkg in pkgs:
    name, version = pkg.split(' ')
    version = version.rstrip()
    print(f'| {name} | {version} |', end='   |\n')
